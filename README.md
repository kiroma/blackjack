# Console Blackjack
A game of Blackjack in console using ncurses!  
![Gameplay screenshot 1](https://i.imgur.com/w4679tk.png)  
![Gameplay screenshot 2](https://i.imgur.com/CNUeDqh.png)  
![Gameplay screenshot 3](https://i.imgur.com/Ojua5gI.png)  
![Gameplay screenshot 4](https://i.imgur.com/3lHUDTK.png)