#include "game.hpp"
#include <curses.h>

constexpr int points[]{2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11};

int countpoints(const std::vector<card> &hand) // For counting points when Aces are in hand
{
	int res = 0;
	for(auto b : hand)
	{
		res += points[b.value];
	}
	return res;
}

int countreal(const std::vector<card> &hand)
{
	int score = 0;
	int tempscore = 0;
	for(card a : hand)
	{
		if(a.value == 12)
		{
			if((tempscore > 21) || (tempscore = countpoints(hand) > 21))
			{
				score += 1;
			}
			else
			{
				score += 11;
			}
		}else
		{
			score += points[a.value];
		}
	}
	return score;
}

int get_bet(const int balance)
{
	int bet;
	bool bet_valid = false;
	erase();
	mvprintw(0, 0, "Balance: %d\nEnter a bet: ", balance);
	refresh();
	echo();
	scanw("%u", &bet);
	noecho();
	if(bet > 0 && bet <= balance)
	{
		bet_valid = true;
	}
	while(!bet_valid)
	{
		erase();
		mvprintw(0, 0, "Balance: %d\nInvalid input\nPlease enter a valid bet: ", balance);
		refresh();
		echo();
		scanw("%u", &bet);
		noecho();
		if(bet > 0 && bet <= balance)
		{
			bet_valid = true;
		}
	}
	return bet;
}

int game(int &balance)
{
	int bet = get_bet(balance);
	pack deck;
	deck.shuffle();
	std::vector<card> hand;
	std::vector<card> house;
	for(unsigned short i = 0; i < 2; ++i)
	{
		house.push_back(deck.hit());
	}
	int score, housescore;
	bool hold = false;
	while(!hold)
	{
		score = countreal(hand);
		housescore = countreal(house);
		rendergame(house, housescore, hand, score, balance);
		if(score > 21)
		{
			printw("\nThe player has lost.\n");
			balance -= bet;
			return -1;
		}
		if(hand.size() == 2 && score == 21)
		{
			printw("\nBlackjack!\n");
			balance += bet;
			return 2;
		}
		switch(getch())
		{
			case 'd':
				if(bet*2 <= balance)
				{
					bet *= 2;
					hand.push_back(deck.hit());
					hold = true;
				}
				break;
			case 'h':
				hand.push_back(deck.hit());
				break;
			case 'g':
				hold = true;
				break;
			case 'q':
				balance -= bet;
				return -2;
		}
	}
	if((score = countreal(hand)) > 21)
	{
		rendergame(house, housescore, hand, score, balance);
		printw("\nThe player has lost.\n");
		balance -= bet;
		return -1;
	}
	while(housescore <= score && housescore < 21)
	{
		house.push_back(deck.hit());
		housescore = countreal(house);
		score = countreal(hand);
	}
	rendergame(house, housescore, hand, score, balance);
	if(housescore > 21)
	{
		printw("\nThe Player has won");
		balance += bet;
		return 1; // win
	}
	else
	{
		if(housescore > score)
		{
			printw("\nThe player has lost");
			balance -= bet;
			return -1; // loss
		}
		else
		{
			if(housescore == score)
			{
				printw("\nTie!");
				return 0; // tie
			}
		}
	}
	balance -= bet;
	return -1;
}

void rendergame(const std::vector<card> &house, const int housescore, const std::vector<card> &player, const int playerscore, const int balance)
{
	constexpr char values[]("234567890JQKA");
	mvprintw(0, 0, "House: \n");
	for(card a : house)
	{
		attron(COLOR_PAIR(a.colour));
		addch(values[a.value]);
		attroff(COLOR_PAIR(a.colour));
	}
	printw("\nScore: %d", housescore);
	printw("\nPlayer: \n");
	for(card a : player)
	{
		attron(COLOR_PAIR(a.colour));
		addch(values[a.value]);
		attroff(COLOR_PAIR(a.colour));
	}
	printw("\nScore: %d\nBalance: %d", playerscore, balance);
	printw("\n(D)Double (H)Hit (G)Stand (Q)Give up");
	refresh();
}
