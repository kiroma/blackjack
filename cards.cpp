#include "cards.hpp"
#include <algorithm>

card::card(const unsigned short value, const unsigned short colour): value(value), colour(colour){}

pack::pack()
{
	for(unsigned short i = 0; i < 4; ++i)
	{
		for(unsigned short j = 0; j < 13; ++j)
		{
			cards.emplace_back(card(j, i));
		}
	}
}

void pack::shuffle()
{
	std::shuffle(cards.begin(), cards.end(), rd);
}

bool pack::empty()
{
	return cards.empty();
}

card pack::hit()
{
	card temp(std::move(cards.back()));
	cards.pop_back();
	return temp;
}
