#pragma once
#include <deque>
#include <random>

struct card{
	unsigned short value;
	unsigned short colour;
	card(const short unsigned int value, const short unsigned int colour);
};

class pack{
	std::deque<card> cards{};
	std::random_device rd;
public:
	pack();
	void shuffle();
	bool empty();
	card hit();
};
