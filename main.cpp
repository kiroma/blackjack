#include <iostream>
#include <vector>
#include "game.hpp"
#include "curser.hpp"

int main() {
	bool should_exit = false;
	curser curser_m;
	int wins = 0, losses = 0, ties = 0, blackjacks = 0;
	int balance = 500;
	while(!should_exit)
	{
		erase();
		mvprintw(0, 0, "Welcome to the console Blackjack!\n(S)Start (Q)Quit\nWins: %d\nLosses: %d\nTies: %d\nBlackjacks: %d\nBalance: %d\n", wins, losses, ties, blackjacks, balance);
		refresh();
		switch(getch())
		{
			case 'q':
				should_exit=true;
				break;
			case 's':
				do
				{
					erase();
					int result;
					result = game(balance);
					switch(result)
					{
						case -1:
							++losses;
							break;
						case 0:
							++ties;
							break;
						case 2:
							++blackjacks; /* fall-thru */
						case 1:
							++wins;
							break;
						default:
							printw("\nGiven up");
							++losses;
							break;
					}
					if(balance > 0)
					{
						printw("\nPress h to try again or any other key to exit");
						refresh();
					}
					else
					{
						printw("\nYou're broke! Press any key to exit.\nStats: \nWins: %d\nLosses: %d\nTies: %d\nBlackjacks: %d\n", wins, losses, ties, blackjacks);
						refresh();
						should_exit = true;
					}
				}while(getch() == 'h' && should_exit != true);
				erase();
				break;
			default:
				break;
		}
		refresh();
	}
    return 0;
}
